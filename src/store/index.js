import Vue from 'vue'
import Vuex from 'vuex'
import { v4 } from 'uuid';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    items:[
      {
        id:v4(),
        name:'Thian Lian Ben',
        email:'misterlianben@gmail.com',
        phonenumber:'0945274573',
        isActive:true
      }
    ]
  },
  mutations: {
    ADD_EMPLOYEE(state,employee){
      state.items.push({
        id:v4(),
        name:employee.name,
        email:employee.email,
        phonenumber:employee.phonenumber,
        isActive:employee.isActive
      })
    },
    UPDATE_EMPLOYEE(state,employee){
      let employeeData = state.items.filter( data => data.id = employee.id )[0];
      employeeData.name = employee.name;
      employeeData.email = employee.email;
      employeeData.phonenumber = employee.phonenumber;
      employeeData.isActive = employee.isActive;
    }
  },
  actions: {
    submitEmployee({commit},employee){
      commit('ADD_EMPLOYEE',employee);
    },
    UpdateEmployee({commit},employee){
      commit('UPDATE_EMPLOYEE',employee);
    }
  },
  modules: {
  },
  getters:{
    getItems(state){
      return state.items
    }
  }
})
